from markdown2 import Markdown
from PyQt5.QtCore import QFile, QIODevice
from PyQt5.QtGui import QFontDatabase
from PyQt5.QtWidgets import QTextEdit

import resources
from resource_files import FileResource


class MainDisplay(QTextEdit):
    def __init__(self, font_size: int = 12, *args, **kwargs):
        super(MainDisplay, self).__init__(*args, **kwargs)

        self.markdown = Markdown(extras=["fenced-code-blocks"])
        self.css_file = QFile(FileResource.MONOKAI)
        self.css_file.open(QIODevice.ReadOnly | QIODevice.Text)
        self.css_data = self.css_file.readAll().data().decode()

        fixed_font = QFontDatabase.systemFont(QFontDatabase.FixedFont)
        fixed_font.setPointSize(font_size)
        self.setFont(fixed_font)
        self.setReadOnly(True)

    def update_text(self, s):
        html = self.markdown.convert(s)
        print(self.css_data)
        html = self.css_data + html
        self.setHtml(html)
