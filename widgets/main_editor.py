from PyQt5.QtGui import QFontDatabase
from PyQt5.QtWidgets import QPlainTextEdit


class MainEditor(QPlainTextEdit):
    def __init__(self, font_size: int = 12, *args, **kwargs):
        super(MainEditor, self).__init__(*args, **kwargs)

        fixed_font = QFontDatabase.systemFont(QFontDatabase.FixedFont)
        fixed_font.setPointSize(font_size)
        self.setFont(fixed_font)

    def update_text(self, s) -> None:
        print("update_text not implemented")
