import sys

from PyQt5.QtWidgets import QApplication

from utils import get_dark_theme
from windows.main_window import MainWindow

if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyle("Fusion")
    app.setPalette(get_dark_theme())
    app.setApplicationName("Sawtooth Notes")

    main_window = MainWindow()
    app.exec_()
