from markdown2 import Markdown
from PyQt5.QtWidgets import (QHBoxLayout, QMainWindow, QPlainTextEdit,
                             QStatusBar, QTextEdit, QWidget)

from widgets import MainDisplay, MainEditor


class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.editor = QPlainTextEdit()
        self.display = QTextEdit()
        self.markdown = Markdown(extras=["fenced-code-blocks"])

        self.editor = MainEditor()
        self.display = MainDisplay()
        self.editor.textChanged.connect(lambda: self.display.update_text(self.editor.toPlainText()))

        layout = QHBoxLayout()
        layout.addWidget(self.editor)
        layout.addWidget(self.display)
        container = QWidget()
        container.setLayout(layout)
        self.setCentralWidget(container)

        self.status_bar = QStatusBar()
        self.setStatusBar(self.status_bar)

        self.show()

    def test(self):
        print("sdljflsdf")
